import Clients.Client;
import Clients.DebugClient;
import Clients.GUIClient;
import Logic.Board;
import Network.ClientConnection;
import Network.ConnectionHandler;
import Network.Messages.Command.Command;
import Network.Messages.Event.Event;
import Network.Messages.Message;
import Servers.ClassicServer;
import Servers.Server;
import javafx.application.Application;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
  public static void main(String[] args) {
    if (args.length == 0) {
      System.err.println("Usage: [--server | --gui | --ai] args...");
      System.exit(-1);
    }

    switch (args[0]) {
      case "--server":
        Server(args);
        break;

      case "--gui":
        Client(args);
        break;

      case "--ai":
        AI(args);
        break;

      default:
        System.err.println("Usage: [--server | --gui | --ai] args...");
        System.exit(-2);
    }
  }

  private static void Server(String[] args) {
    if (args.length != 6) {
      System.err.println("Command line usage for server: --server <IP> <Port> <players> <Board file> <Deck file>");
      System.exit(1);
    }

    ServerSocket serverSocket;
    try {
      serverSocket = new ServerSocket(Integer.valueOf(args[2]));
    } catch (IOException e) {
      System.exit(-2);
      return;
    }

    int players;

    try {
      players = Integer.valueOf(args[3]);
    } catch (NumberFormatException e) {
      System.err.println("Expected player number as 3rd server argument!");
      System.exit(-1);
      return;
    }

    if (players < 2) {
      System.err.println("A game needs to be played with at least 2 players.");
      System.exit(-1);
    }

    ClassLoader loader = ClassLoader.getSystemClassLoader();
    Board board = Board.fromInputStream(loader.getResourceAsStream(args[4]));

    if (board == null) {
      System.err.println("Board file does not exist or is malformed. Aborting...");
      System.exit(-1);
    }

    final long seed = System.currentTimeMillis();
    InputStream deck = loader.getResourceAsStream(args[5]);
    ConnectionHandler connections = new ConnectionHandler(players, serverSocket);

    Server server = new ClassicServer(players, seed, board, deck, connections);
    server.startGame();

    while (server.isActive()) {
      Command command = connections.getNextCommand();
      if (command != null) {
        command.update(server);
      }
    }
  }

  private static void Client(String[] args) {
    Application.launch(ClientApplication.class, args);
  }

  private static void AI(String[] args) {
    System.err.println("Not implemented!");
    System.exit(-3);
  }
}
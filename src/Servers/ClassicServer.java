package Servers;

import Logic.Board;
import Logic.DeckInfo;
import Logic.State.ServerModel;
import Network.ConnectionHandler;
import Network.Messages.Command.*;

import java.io.InputStream;

public class ClassicServer implements Server {
  private ServerModel gameState;
  private ConnectionHandler connections;
  private boolean activeflag;

  public ClassicServer(int players, long seed, Board board, InputStream deckstream, ConnectionHandler connections) {
    this.connections = connections;
    this.gameState = new ServerModel(players, seed, board, new DeckInfo(deckstream));
  }

  @Override
  public void startGame() {
    connections.connectAll();
  }

  @Override
  public boolean isActive() {
    return !gameState.isFinished();
  }

  @Override
  public void onCommand(Attack attack) {
    gameState.handle(connections, attack);
  }

  @Override
  public void onCommand(ConnectionEstablished connectionEstablished) {
    gameState.handle(connections, connectionEstablished);
  }

  @Override
  public void onCommand(ConnectionFailed connectionFailed) {
    gameState.handle(connections, connectionFailed);
  }

  @Override
  public void onCommand(Defend defend) {
    gameState.handle(connections, defend);
  }

  @Override
  public void onCommand(EndCapturing endCapturing) {
    gameState.handle(connections, endCapturing);
  }

  @Override
  public void onCommand(EndTurn endTurn) {
    gameState.handle(connections, endTurn);
  }

  @Override
  public void onCommand(JoinRequest joinRequest) {
    gameState.handle(connections, joinRequest);
  }

  @Override
  public void onCommand(MoveUnits moveUnits) {
    gameState.handle(connections, moveUnits);
  }

  @Override
  public void onCommand(NoTrade noTrade) {
    gameState.handle(connections, noTrade);
  }

  @Override
  public void onCommand(PlaceUnits placeUnits) {
    gameState.handle(connections, placeUnits);
  }

  @Override
  public void onCommand(PlayerDisconnection playerDisconnection) {
    gameState.handle(connections, playerDisconnection);
  }

  @Override
  public void onCommand(Siege siege) {
    gameState.handle(connections, siege);
  }

  @Override
  public void onCommand(Trade trade) {
    gameState.handle(connections, trade);
  }
}

package Servers;

import Network.Messages.Command.*;

public interface Server {
  void startGame();

  boolean isActive();

  void onCommand(Attack attack);
  void onCommand(ConnectionEstablished connectionEstablished);
  void onCommand(ConnectionFailed connectionFailed);
  void onCommand(Defend defend);
  void onCommand(EndCapturing endCapturing);
  void onCommand(EndTurn endTurn);
  void onCommand(JoinRequest joinRequest);
  void onCommand(MoveUnits moveUnits);
  void onCommand(NoTrade noTrade);
  void onCommand(PlaceUnits placeUnits);
  void onCommand(PlayerDisconnection playerDisconnection);
  void onCommand(Siege siege);
  void onCommand(Trade trade);
}

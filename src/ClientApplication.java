import Clients.GUIClient;
import Network.ClientConnection;
import Network.Messages.Event.Event;
import Network.Messages.Message;
import Network.Messages.Terminate;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientApplication extends Application {
  private Thread networkingThread;
  private GUIClient gui;
  private Stage stage;

  @FXML private TextField ipField;
  @FXML private TextField portField;
  @FXML private TextField nameField;

  public ClientApplication() {
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.stage = primaryStage;

    FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemClassLoader().getResource("Intro.fxml"));
    loader.setController(this);

    stage.setScene(new Scene(loader.load()));
    stage.sizeToScene();
    stage.centerOnScreen();
    stage.setResizable(false);
    stage.show();
  }

  @FXML
  private void connect() throws Exception {
    String IP = ipField.getText();
    int port = Integer.parseInt(portField.getText());
    String name = nameField.getText();

    BlockingQueue<Message> queue = new LinkedBlockingQueue<>();
    ClientConnection connection = new ClientConnection(queue, IP, port);
    gui = new GUIClient(connection, queue, name, stage);

    networkingThread = new Thread(() -> {
      try {
        connection.connect();
        System.out.println("Connected to the server.");
      } catch (IOException e) {
        e.printStackTrace();
        System.err.println("Failed connecting. The program will now exit.");
        System.exit(-4);
      }

      while (gui.isActive()) {
        try {
          Message message = queue.take();

          if (message instanceof Terminate) {
            break;
          }

          ((Event) message).update(gui);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      connection.disconnect();
    });

    networkingThread.start();
  }

  @Override
  public void stop() {
    if (networkingThread != null) {
      try {
        networkingThread.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

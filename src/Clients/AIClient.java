package Clients;

import Network.Messages.Event.*;

public class AIClient implements Client {
  @Override
  public boolean isActive() {
    return false;
  }

  @Override
  public void onEvent(AttackDeclared event) {

  }

  @Override
  public void onEvent(AttackFinished event) {

  }

  @Override
  public void onEvent(BaseReinforcements event) {

  }

  @Override
  public void onEvent(BonusReinforcements event) {

  }

  @Override
  public void onEvent(CapturableDrawn event) {

  }

  @Override
  public void onEvent(CapturePhaseEnd event) {

  }

  @Override
  public void onEvent(CardDrawn event) {

  }

  @Override
  public void onEvent(DiceThrows event) {

  }

  @Override
  public void onEvent(GameStart event) {

  }

  @Override
  public void onEvent(GameInfo event) {

  }

  @Override
  public void onEvent(GameWon event) {

  }

  @Override
  public void onEvent(IllegalAction event) {

  }

  @Override
  public void onEvent(PlayerJoined event) {

  }

  @Override
  public void onEvent(PlayerLeft event) {

  }

  @Override
  public void onEvent(ReceivedStartingUnits event) {

  }

  @Override
  public void onEvent(SetupUnit event) {

  }

  @Override
  public void onEvent(TradeDeclined tradedeclined) {

  }

  @Override
  public void onEvent(UnitMove event) {

  }

  @Override
  public void onEvent(UnitSiege event) {

  }

  @Override
  public void onEvent(UnitsPlaced event) {

  }

  @Override
  public void onEvent(UnknownCardDrawn event) {

  }
}

package Clients;

import Network.Messages.Event.*;

// Implemented as an Event Listener.
public interface Client {
  boolean isActive();

  void onEvent(AttackDeclared event);
  void onEvent(AttackFinished event);
  void onEvent(BaseReinforcements event);
  void onEvent(BonusReinforcements event);
  void onEvent(CapturableDrawn event);
  void onEvent(CapturePhaseEnd event);
  void onEvent(CardDrawn event);
  void onEvent(DiceThrows event);
  void onEvent(GameStart event);
  void onEvent(GameInfo event);
  void onEvent(GameWon event);
  void onEvent(IllegalAction event);
  void onEvent(PlayerJoined event);
  void onEvent(PlayerLeft event);
  void onEvent(ReceivedStartingUnits event);
  void onEvent(SetupUnit event);
  void onEvent(TradeDeclined tradedeclined);
  void onEvent(UnitMove event);
  void onEvent(UnitSiege event);
  void onEvent(UnitsPlaced event);
  void onEvent(UnknownCardDrawn event);
}

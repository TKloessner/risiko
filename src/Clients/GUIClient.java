package Clients;

import GUI.BoardView;
import GUI.CardView;
import GUI.GUIView;
import GUI.ScoreBoard;
import Logic.ClientModel;
import Network.ClientConnection;
import Network.Messages.Command.JoinRequest;
import Network.Messages.Event.*;
import Network.Messages.Message;
import Network.Messages.Terminate;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

// TODO implement!
public class GUIClient implements Client {
  // Constructed when the GameInfo event is received
  private Stage stage;
  private ClientModel gameState;
  private ClientConnection connection;
  private BlockingQueue<Message> queue;
  private GUIView gui;
  private boolean activeflag;
  private String name;

  public GUIClient(ClientConnection connection, BlockingQueue<Message> queue, String name, Stage stage)
      throws IOException
  {
    this.stage = stage;
    this.gameState = null;
    this.connection = connection;
    this.queue = queue;
    this.activeflag = true;
    this.name = name;
  }

  @Override
  public synchronized boolean isActive() {
    return activeflag;
  }

  @Override
  public void onEvent(AttackDeclared event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(AttackFinished event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(BaseReinforcements event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(BonusReinforcements event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(CapturableDrawn event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(CapturePhaseEnd event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(CardDrawn event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(DiceThrows event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(GameStart event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(GameInfo event) {
    try {
      // NOTE: The deck info is discarded, an AI could use this however.
      this.gameState = new ClientModel(event.players, event.board, event.ID);
      this.gui = new GUIView(stage, queue, event, connection, gameState);

      gameState.register(gui);
      connection.enqueueMessage(new JoinRequest(name));
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(-5);
    }
  }

  @Override
  public void onEvent(GameWon event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(IllegalAction event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(PlayerJoined event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(PlayerLeft event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(ReceivedStartingUnits event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(SetupUnit event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(TradeDeclined event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(UnitMove event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(UnitSiege event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(UnitsPlaced event) {
    gameState.handle(event);
  }

  @Override
  public void onEvent(UnknownCardDrawn event) {
    gameState.handle(event);
  }
}

package Clients;

import Network.ClientConnection;
import Network.Messages.Command.JoinRequest;
import Network.Messages.Event.*;

// A Debug Visitors that logs input events
public class DebugClient implements Client {
  private ClientConnection connection;
  private String clientname;
  private boolean active;

  public DebugClient(ClientConnection connection, String clientname) {
    this.connection = connection;
    this.active = true;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void onEvent(AttackDeclared event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(AttackFinished event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(BaseReinforcements event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(BonusReinforcements event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(CapturableDrawn event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(CapturePhaseEnd event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(CardDrawn event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(DiceThrows event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(GameStart event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(GameInfo event) {
    System.out.println(event.toString());
    connection.enqueueMessage(new JoinRequest(clientname));
  }

  @Override
  public void onEvent(GameWon event) {
    System.out.println(event.toString());
    active = false;
  }

  @Override
  public void onEvent(IllegalAction event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(PlayerJoined event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(PlayerLeft event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(ReceivedStartingUnits event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(SetupUnit event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(TradeDeclined tradedeclined) {

  }

  @Override
  public void onEvent(UnitMove event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(UnitSiege event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(UnitsPlaced event) {
    System.out.println(event.toString());
  }

  @Override
  public void onEvent(UnknownCardDrawn event) {
    System.out.println(event.toString());
  }
}

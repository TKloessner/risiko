package Network;

import Network.Messages.Message;
import Network.Messages.Ping;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ClientConnection extends Connection {
  private final String IP;
  private final int port;

  public ClientConnection(BlockingQueue<Message> queue, String IP, int port) {
    super(queue);
    this.IP = IP;
    this.port = port;
  }

  public void connect() throws IOException {
    synchronized (this) {
      // Oops!
      if (socket != null) {
        throw new IllegalStateException("Tried to connect while already connected. A prior shutdown is necessary.");
      }
    }

    terminate = false;
    socket = new Socket(IP, port);

    try {
      socket.setSoTimeout(TIMEOUT);
      objectoutput = new ObjectOutputStream(socket.getOutputStream());
      objectinput = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
    } catch (IOException e) {
      try {
        socket.close();
      } catch (IOException e2) {
        e2.printStackTrace();
      }

      socket = null;
      objectinput = null;
      objectoutput = null;
      throw e;
    }

    // Start the dedicated ping thread
    (thread = new Thread(this::ping)).start();
  }

  private void ping() {
    try {
      for (;;) {
        synchronized (this) {
          if (terminate) {
            socket.shutdownOutput();
            return;
          }

          // Send a pending message
          if (!pending.isEmpty()) {
            sendMessage(pending.poll());
          }

          // Read objects until a ping is encountered
          for (;;) {
            Message message = receiveMessage();
            if (message instanceof Ping) {
              sendMessage(message);
              break; // Use the ping to release the lock periodically
            } else {
              received.offer(message); // Concurrent queue
            }
          }
        }
      }
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }

      socket = null;
      objectinput = null;
      objectoutput = null;
    }
  }
}
package Network;

import Network.Messages.Command.Command;
import Network.Messages.Command.ConnectionEstablished;
import Network.Messages.Command.ConnectionFailed;
import Network.Messages.Command.PlayerDisconnection;
import Network.Messages.Message;
import Network.Messages.Ping;

import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;

public class ServerConnection extends Connection {
  private ServerSocket server;
  private int ID;

  ServerConnection(BlockingQueue<Message> queue, ServerSocket server, int ID) {
    super(queue);
    this.server = server;
    this.ID = ID;
  }

  public void connect() throws IOException {
    terminate = false;
    socket = server.accept();

    try {
      socket.setSoTimeout(TIMEOUT);
      objectoutput = new ObjectOutputStream(socket.getOutputStream());
      objectinput = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
    } catch (IOException e) {
      try {
        socket.close();
      } catch (IOException io) {
        io.printStackTrace();
      }

      socket = null;
      objectinput = null;
      objectoutput = null;
      throw e;
    }

    (thread = new Thread(this::ping)).start();
  }

  private void ping() {
    try {
      for (;;) {
        synchronized (this) {
          if (terminate) {
            socket.shutdownOutput();
            return;
          }

          // Send a pending message
          if (!pending.isEmpty()) {
            sendMessage(pending.poll());
          }

          // Send a ping
          sendMessage(new Ping());

          // Receive until a ping response
          for (;;) {
            Message message = receiveMessage();
            if (message instanceof Ping) {
              break;
            } else {
              Command command = (Command) message;
              command.setSenderID(ID);
              received.offer(command);
            }
          }
        }
      }
    } catch (IOException | ClassNotFoundException e) {
      received.offer(new PlayerDisconnection(ID));
    } finally {
      try {
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }

      socket = null;
      objectinput = null;
      objectoutput = null;
    }
  }
}
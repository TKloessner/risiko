package Network.Messages.Event;

import Clients.Client;

// This event is sent when the turning player send a player intends to attack a capturable from an enemy player
public class AttackDeclared extends Event {
  public final String capfrom;
  public final String capagainst;
  public final int units;

  public AttackDeclared(String capfrom, String capagainst, int units) {
    this.capfrom = capfrom;
    this.capagainst = capagainst;
    this.units = units;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;
import Logic.Card;

/**
 * Received when a card was drawn from the deck.
 */
public class CardDrawn extends Event {
  public final Card card;

  public CardDrawn(Card card) {
    this.card = card;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

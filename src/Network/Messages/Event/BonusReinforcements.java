package Network.Messages.Event;

import Clients.Client;

/**
 * Represents the reinforcements the turning player gets due to cards traded.
 * @see BaseReinforcements
 */
public class BonusReinforcements extends Event {
  public final int cardbonus;

  public BonusReinforcements(int cardbonus) {
    this.cardbonus = cardbonus;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

public class SetupUnit extends Event {
  public String capturable;

  public SetupUnit(String capturable) {
    this.capturable = capturable;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;
import Logic.Player;

/**
 * This event is sent to a player at the beginning of the game when he is assigned a capturable that he controls.
 * Initially, the capturable holds one unit of the controlling player.
 *
 * {@link Logic.Capturable#give(Player)}
 */
public class CapturableDrawn extends Event {
  public final int ID;
  public final String capturable;

  public CapturableDrawn(int ID, String cap) {
    this.ID = ID;
    this.capturable = cap;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

// This event is sent when an attack from the turning player was executed.
// It holds the result of the battle, stating lost units on both sides.
public class AttackFinished extends Event {
  public final int attackerUnitsLost;
  public final int defenderUnitsLost;

  public AttackFinished(int attackerUnitsLost, int defenderUnitsLost) {
    this.attackerUnitsLost = attackerUnitsLost;
    this.defenderUnitsLost = defenderUnitsLost;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

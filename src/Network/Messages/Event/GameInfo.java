package Network.Messages.Event;

import Clients.Client;
import Logic.Board;
import Logic.DeckInfo;

/**
 * Sent to a client as the first event once the connection has been estalished.
 * Holds information related to the game settings, including board, number of player participating,
 * the ID a client is refrred by, and the shape of the deck.
 *
 * @see Board
 * @see DeckInfo
 */
public class GameInfo extends Event {
  public final int players;
  public final int ID;
  public final Board board;
  public final DeckInfo deckInfo;

  public GameInfo(int players, int ID, Board board, DeckInfo deckInfo) {
    this.players = players;
    this.ID = ID;
    this.board = board;
    this.deckInfo = deckInfo;
  }

  public GameInfo(int players, int ID, Board board, int infantries, int cavalries, int cannons, int jokers) {
    this.players = players;
    this.ID = ID;
    this.board = board;
    this.deckInfo = new DeckInfo(infantries, cavalries, cannons, jokers);
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

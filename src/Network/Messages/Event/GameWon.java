package Network.Messages.Event;

import Clients.Client;

/**
 * Sent upon game end when a player fulfills a game winning condition.
 */
public class GameWon extends Event {
  public final int winnerID;

  public GameWon(int playerID) {
    this.winnerID = playerID;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

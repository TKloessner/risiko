package Network.Messages.Event;

import Clients.Client;

/**
 * Sent when the capture phase is terminated by the turning player.
 */
public class CapturePhaseEnd extends Event {
  public CapturePhaseEnd() {
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

public class TradeDeclined extends Event {
  public TradeDeclined() {
  }

  @Override
  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

/**
 * Holds the result of the dice throws determining if an attack from the turning player on an enemy's
 * capturable was successful or not. Sent after the corresponding attack and defend events were sent.
 *
 * @see Network.Messages.Command.Attack
 * @see Network.Messages.Command.Defend
 */
public class DiceThrows extends Event {
  public final int[] attackervalues;
  public final int[] defendervalues;

  public DiceThrows(int[] attackervalues, int[] defendervalues) {
    this.attackervalues = attackervalues;
    this.defendervalues = defendervalues;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

/**
 * Sent when the required number of players connected to the server and the game is ready to start.
 */
public class GameStart extends Event {
  public GameStart() {
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

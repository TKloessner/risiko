package Network.Messages.Event;

import Clients.Client;

/**
 * Sent to a player at game start.
 * The player referred to gets a set amount of starting units he must place in the setup phase of the game.
 */
public class ReceivedStartingUnits extends Event {
  public final int amount;
  public final int recipientID;

  public ReceivedStartingUnits(int amount, int recipientID) {
    this.amount = amount;
    this.recipientID = recipientID;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

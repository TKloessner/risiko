package Network.Messages.Event;

import Clients.Client;

/**
 * Represents the units the turning player receives during reinforcement phase due to his territorial
 * and continental reinforcement bonuses, the optional bonus due to card trading not included.
 *
 * @see BonusReinforcements
 */
public class BaseReinforcements extends Event {
  public final int territorial;
  public final int continental;

  public BaseReinforcements(int territorial, int continental) {
    this.territorial = territorial;
    this.continental = continental;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

/**
 * Sent after successful capture of an implicit capturable of the turning player. The turning player takes control over
 * the capturable with a specified amount of units.
 */
public class UnitSiege extends Event {
  public final int units;

  public UnitSiege(int units) {
    this.units = units;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

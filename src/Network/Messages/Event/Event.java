package Network.Messages.Event;

import Clients.Client;
import Network.Messages.Message;

/**
 * The base class for any game related occurrence that is notified to the client by the server.
 * A client can derive the game state by simply observing these events.
 */
public abstract class Event extends Message {
  public abstract void update(Client client);
}
package Network.Messages.Event;

import Clients.Client;

/**
 * Sent when the single receiving client tried to announce an action that is not allowed during the current game state.
 * Such an event is necessary to counter cheating attempts, i.e. attacking a capturable with an insufficient amount
 * of units.
 */
public class IllegalAction extends Event {
  public final String description;

  public IllegalAction(String description) {
    this.description = description;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

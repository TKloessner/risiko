package Network.Messages.Event;

import Clients.Client;

/**
 * Sent in the Move Phase when the turning player moves units from a capturable he controls to another capturable
 * he controls.
 */
public class UnitMove extends Event {
  public final String origin;
  public final String target;
  public final int amount;

  public UnitMove(String origin, String target, int amount) {
    this.origin = origin;
    this.target = target;
    this.amount = amount;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

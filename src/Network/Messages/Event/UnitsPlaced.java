package Network.Messages.Event;

import Clients.Client;

/**
 * Sent during reinforcement phase. Represents that the turning player sent a sepcified amount of his free units on
 * one of his capturables.
 */
public class UnitsPlaced extends Event {
  public final int amount;
  public final String cap;

  public UnitsPlaced(int amount, String cap) {
    this.amount = amount;
    this.cap = cap;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

package Network.Messages.Event;

import Clients.Client;

/**
 * Indicates that another participating client left the game.
 */
public class PlayerLeft extends Event {
  public final int ID;

  public PlayerLeft(int ID) {
    this.ID = ID;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

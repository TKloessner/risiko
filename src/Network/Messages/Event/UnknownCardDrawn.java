package Network.Messages.Event;

import Clients.Client;

/**
 * Specifies that the turning player drew a card in his draw phase. the card is only known to himself.
 */
public class UnknownCardDrawn extends Event {
  public UnknownCardDrawn() {
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

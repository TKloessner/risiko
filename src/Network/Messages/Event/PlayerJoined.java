package Network.Messages.Event;

import Clients.Client;

/**
 * Sent when a new player joins the game. The joining player is participating in the game, whereas a passive observer
 * only watches the game without having any influence on the game state.
 */
public class PlayerJoined extends Event {
  public final int ID;
  public final String name;

  public PlayerJoined(int ID, String name) {
    this.ID = ID;
    this.name = name;
  }

  public void update(Client client) {
    client.onEvent(this);
  }
}

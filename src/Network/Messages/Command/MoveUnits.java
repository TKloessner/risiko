package Network.Messages.Command;

import Servers.Server;

public class MoveUnits extends Command {
  public final String originname;
  public final String targetname;
  public final int amount;

  public MoveUnits(String originname, String targetname, int amount) {
    this.originname = originname;
    this.targetname = targetname;
    this.amount = amount;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

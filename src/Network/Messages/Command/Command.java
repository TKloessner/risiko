package Network.Messages.Command;

import Network.Messages.Message;
import Servers.Server;

public abstract class Command extends Message {
  private int senderID;

  public int getSenderID() {
    return senderID;
  }

  public void setSenderID(int ID) {
    senderID = ID;
  }

  public abstract void update(Server server);
}
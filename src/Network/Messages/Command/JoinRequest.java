package Network.Messages.Command;

import Servers.Server;

public class JoinRequest extends Command {
  public final String playername;

  public JoinRequest(String name) {
    this.playername = name;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

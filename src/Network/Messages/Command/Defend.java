package Network.Messages.Command;

import Servers.Server;

public class Defend extends Command {
  public final int units;

  public Defend(int units) {
    this.units = units;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

package Network.Messages.Command;

import Logic.Card;
import Servers.Server;

public class Trade extends Command {
  public final Card[] cards;

  public Trade(Card[] cards) {
    this.cards = cards;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

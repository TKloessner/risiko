package Network.Messages.Command;

import Servers.Server;

public class PlayerDisconnection extends Command {
  public PlayerDisconnection(int ID) {
    setSenderID(ID);
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

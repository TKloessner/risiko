package Network.Messages.Command;

import Servers.Server;

public class EndTurn extends Command {
  public EndTurn() {
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

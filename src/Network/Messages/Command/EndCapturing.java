package Network.Messages.Command;

import Servers.Server;

public class EndCapturing extends Command {
  EndCapturing() {
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

package Network.Messages.Command;

import Servers.Server;

public class PlaceUnits extends Command {
  public final String capturablename;
  public final int amount;

  public PlaceUnits(String capturablename, int amount) {
    this.capturablename = capturablename;
    this.amount = amount;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

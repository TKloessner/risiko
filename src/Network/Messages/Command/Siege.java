package Network.Messages.Command;

import Servers.Server;

public class Siege extends Command {
  public final int units;

  public Siege(int units) {
    this.units = units;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

package Network.Messages.Command;

import Servers.Server;

public class Attack extends Command {
  public final String attackername;
  public final String targetname;
  public final int units;

  public Attack(String attackername, String targetname, int units) {
    this.attackername = attackername;
    this.targetname = targetname;
    this.units = units;
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

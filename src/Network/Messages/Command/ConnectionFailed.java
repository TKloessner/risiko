package Network.Messages.Command;

import Servers.Server;

public class ConnectionFailed extends Command {
  public ConnectionFailed(int ID) {
    setSenderID(ID);
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

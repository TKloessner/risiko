package Network.Messages.Command;

import Servers.Server;

public class ConnectionEstablished extends Command {
  public ConnectionEstablished(int ID) {
    setSenderID(ID);
  }

  @Override
  public void update(Server server) {
    server.onCommand(this);
  }
}

package Network;

import Network.Messages.Message;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class Connection {
  static final int TIMEOUT = 10000;

  protected Socket socket;

  protected final BlockingQueue<Message> pending;
  protected final BlockingQueue<Message> received;
  protected Thread thread;
  protected boolean terminate;

  protected ObjectInputStream objectinput;
  protected ObjectOutputStream objectoutput;

  Connection(BlockingQueue<Message> received) {
    this.pending = new LinkedBlockingQueue<>();
    this.received = received;
    this.socket = null;
    this.terminate = false;
    this.thread = null;
  }

  public void disconnect() {
    synchronized (this) {
      if (socket == null) {
        return;
      }

      terminate = true;
    }

    try {
      thread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void enqueueMessage(Message message) {
    pending.offer(message);
  }

  protected void sendMessage(Message message) throws IOException {
    objectoutput.writeObject(message);
    objectoutput.flush();
  }

  protected Message receiveMessage() throws IOException, ClassNotFoundException {
    return (Message) objectinput.readObject();
  }
}
package Network;

import Logic.Player;
import Network.Messages.Command.Command;
import Network.Messages.Command.ConnectionEstablished;
import Network.Messages.Command.ConnectionFailed;
import Network.Messages.Event.Event;
import Network.Messages.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ConnectionHandler {
  private ServerConnection[] pingedConnections;
  private BlockingQueue<Message> messages;
  private ServerSocket server;

  public ConnectionHandler(int connections, ServerSocket server) {
    this.pingedConnections = new ServerConnection[connections];
    this.messages = new LinkedBlockingQueue<>();
    this.server = server;
  }

  public void connectAll() {
    for (int i = 0; i != pingedConnections.length; ++i) {
      connect(i);
    }
  }

  public void connect(int ID) {
    new Thread(() -> {
      ServerConnection connection = new ServerConnection(messages, server, ID);
      try {
        connection.connect();
        pingedConnections[ID] = connection;
        messages.offer(new ConnectionEstablished(ID));
      } catch (IOException e) {
        messages.offer(new ConnectionFailed(ID));
      }
    }).start();
  }

  public Command getNextCommand() {
    try {
      return (Command) messages.take();
    } catch (InterruptedException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void sendEvent(Player player, Event event) {
    sendEvent(player.ID, event);
  }

  public void sendEvent(int ID, Event event) {
    if (pingedConnections[ID] != null) {
      pingedConnections[ID].enqueueMessage(event);
    }
  }

  public void sendEventToAll(Event event) {
    for (int i = 0; i < pingedConnections.length; ++i) {
      sendEvent(i, event);
    }
  }

  public void removeConnection(int ID) {
    pingedConnections[ID] = null;
  }

  public void shutdownConnections() {
    for (ServerConnection pingedConnection : pingedConnections) {
      if (pingedConnection != null) {
        pingedConnection.disconnect();
      }
    }
  }
}
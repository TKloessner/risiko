package Logic;

public interface ClientModelObserver {
  // void onPlayerJoined(Player player);
  // void onSetupStart();
  // void onSetUnit(Capturable capturable);
  // void onGameStart();

  // void onCardDrawn(Card card);
  // void onBaseReinforcements(int territorial, int continental);
  // void onBonusReinforcements(int bonus);
  // void onReinforcement(Capturable reinforced, int amount);
  // void onCapturePhaseStart();
  // void onAttackDeclaration(Capturable attacker, Capturable defender, int attackeramount);
  // void onDefense(int defenderamount);
  // void onThrow(int[] attackerthrow, int[] defenderthrow);
  // void onAttackCompletion(Capturable attacker, Capturable defender, int killedAttackers, int killedDefenders);
  // void onSiege(Capturable sieged, int amount);
  // void onCapturePhaseEnd();
  // void onMove(Capturable source, Capturable destination, int amount);
  // void onTurnEnd();

  void onPleaseTrade();
  void onPleaseReinforce();
  void onPleaseCapture();
  void onPleaseDefend(Capturable attacker, Capturable defender);
  void onPleaseSiege(Capturable attacker, Capturable defender);
  void onPleaseMove();

  void onPlayerJoined(Player player);

  void onAttackDeclaration(Capturable attacker, Capturable target, int units);
  void onAttackCompletion(Capturable attacker, Capturable defnder);
  void onUnitsKilled(Capturable attacker, Capturable defender, int killedAttacking, int killedDefending);
  void onBaseReinforcements(int territorial, int continental);
  void onBonusReinforcements(int bonus);
  void onCapturableDrawn(Player player, Capturable capturable);
  void onCapturePhaseEnd();
  void onCardDrawn(Card card);
  void onThrow(int[] attackerthrows, int[] defenderthrows);
  void onGameStart();
  void onGameEnd(Player winner);
  void onIllegal();
  void onReceivedStartingUnits(Player player, int amount);
  void onTradeDeclined();
  void onUnitsMoved(Capturable from, Capturable to, int units);
  void onSiege(Capturable winner, Capturable loser, int units);
  void onUnitsPlaced(Capturable capturable, int units);
  void onAwaitSiege(Capturable attacker, Capturable defender);
}

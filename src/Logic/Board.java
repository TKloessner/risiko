package Logic;

import Utility.Graph;
import com.sun.istack.internal.Nullable;

import java.io.InputStream;
import java.io.Serializable;
import java.util.*;

public class Board implements Serializable {
  private Graph<Capturable> graph;
  private Map<String, Capturable> byname;

  private Board(int nodes) {
    this.graph = new Graph<>();
    this.byname = new HashMap<>(nodes);
  }

  @Nullable
  public Capturable findCapturableByName(String name) {
    return byname.get(name);
  }

  public boolean areAdjacent(Capturable a, Capturable b) {
    return graph.Successors(a).contains(b);
  }

  @Nullable
  public Player getWinner() {
    Player p = graph.Nodes().get(0).getOwner();

    for (Capturable c : graph.Nodes()) {
      if (c.getOwner() != p) {
        return null;
      }
    }

    return p;
  }

  public int getNumNodes() {
    return graph.getNumNodes();
  }

  public List<Capturable> Nodes() {
    return graph.Nodes();
  }

  public List<Capturable> getOwnerPath(Capturable source, Capturable target, Player player) {
    if (source.getOwner() != player || target.getOwner() != player) {
      return null;
    }

    Map<Capturable, Capturable> predecessors = new HashMap<>();
    predecessors.put(source, null);
    Set<Capturable> visited = new HashSet<>();
    Stack<Capturable> stack = new Stack<>();
    stack.push(source);

    while (!stack.empty()) {
      Capturable node = stack.pop();
      visited.add(node);

      // Reconstruct the path from the predecessor map
      if (node == target) {
        // Push onto stack to reverse order
        Stack<Capturable> reverse = new Stack<>();

        do {
          reverse.push(node);
          node = predecessors.get(node);
        } while (node != null);

        // Construct from the stack
        List<Capturable> path = new ArrayList<>();

        while (!reverse.isEmpty()) {
          path.add(reverse.pop());
        }

        return path;
      }

      for (Capturable post : graph.Successors(node)) {
        if (!visited.contains(post) && post.getOwner() == player) {
          stack.push(post);
          predecessors.put(post, node);
        }
      }
    }

    return null;
  }

  @Nullable
  public static Board fromInputStream(InputStream input) {
    Scanner s = new Scanner(input);

    // Parse node count
    int nodes;
    try {
      nodes = s.nextInt();
    } catch (InputMismatchException e) {
      System.err.println("Expected node count!");
      return null;
    }

    Board board = new Board(nodes);

    // Parse node names and create nodes
    for (int i = 0; i < nodes; ++i) {
      board.addNode(s.next());
    }

    s.nextLine();
    s.nextLine(); // One blank line

    // Parse edge beginning marker
    try {
      if (!s.nextLine().equals("Edges:")) {
        System.err.println("Expected \"Edges:\"!");
        return null;
      }
    } catch (NoSuchElementException e) {
      System.err.println("Unexpected end of file, expected \"Edges:\"!");
      return null;
    }

    // Parse edges
    while (s.hasNextLine()) {
      Scanner line = new Scanner(s.nextLine());

      String nodename;

      try {
        nodename = line.next();
      } catch (NoSuchElementException e) {
        System.err.println("Unexpected end of file, expected capturable!");
        return null;
      }

      try {
        if (!line.next().equals("-")) {
          System.err.println("Unexpected character, expected \"-\"!");
          return null;
        }
      } catch (NoSuchElementException e) {
        System.err.println("Unexpected end of file, expected \"-\"!");
        return null;
      }

      while (line.hasNext()){
        try {
          board.addSuccessor(nodename, line.next());
        } catch (NoSuchElementException e) {
          System.err.println("Unexpected end of file, expected capturable!");
          return null;
        }
      }
    }

    return board;
  }

  private void addNode(String string) {
    Capturable capturable = new Capturable(string);
    byname.put(string, capturable);
    graph.addNode(capturable);
  }

  private void addSuccessor(String node, String successor) {
    graph.addSuccessorToNode(findCapturableByName(node), findCapturableByName(successor));
  }
}
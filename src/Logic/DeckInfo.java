package Logic;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Scanner;

public class DeckInfo implements Serializable {
  public int infantries;
  public int cavalries;
  public int cannons;
  public int jokers;
  public int total;

  public DeckInfo(int infantries, int cavalries, int cannons, int jokers) {
    this.infantries = infantries;
    this.cavalries = cavalries;
    this.cannons = cannons;
    this.jokers = jokers;
    this.total = infantries + cavalries + cannons + jokers;
  }

  public DeckInfo(InputStream input) {
    this.infantries = 0;
    this.cavalries = 0;
    this.cannons = 0;
    this.jokers = 0;

    Scanner s = new Scanner(input);

    while (s.hasNext()) {
      String type = s.next();
      int amount = s.nextInt();

      switch (type) {
        case "INFANTRY:":
          infantries += amount;
          break;

        case "CANNON:":
          cannons += amount;
          break;

        case "CAVALRY:":
          cavalries += amount;
          break;

        case "JOKER:":
          jokers += amount;
          break;
      }
    }

    total = infantries + cavalries + cannons + jokers;
  }
}

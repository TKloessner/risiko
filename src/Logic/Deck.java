package Logic;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
  private DeckInfo description;
  private ArrayList<Card> cards;

  private void init() {
    this.cards = new ArrayList<>(description.total);

    for (int i = 0; i < description.infantries; ++i) {
      cards.add(Card.INFANTRY);
    }

    for (int i = 0; i < description.cavalries; ++i) {
      cards.add(Card.CAVALRY);
    }

    for (int i = 0; i < description.cannons; ++i) {
      cards.add(Card.CANNON);
    }

    for (int i = 0; i < description.jokers; ++i) {
      cards.add(Card.JOKER);
    }
  }

  public Deck(InputStream input) {
    this.description = new DeckInfo(input);
    init();
  }

  public Deck(int infantries, int cavalries, int cannons, int jokers) {
    this.description = new DeckInfo(infantries, cavalries, cannons, jokers);
    init();
  }

  public Deck(DeckInfo description) {
    this.description = description;
    init();
  }

  public void shuffle(Random rng) {
    Collections.shuffle(cards, rng);
  }

  public void shuffleIn(Random rng, Card[] in) {
    assert(in.length == 3);
    cards.add(in[0]);
    cards.add(in[1]);
    cards.add(in[2]);
    shuffle(rng);
  }

  public Card draw() {
    return cards.remove(cards.size() - 1);
  }

  public DeckInfo getDescription() {
    return description;
  }
}

package Logic.State;

import Logic.Capturable;
import Logic.Player;
import Network.ConnectionHandler;
import Network.Messages.Command.PlaceUnits;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.SetupUnit;

public class Setup extends State {
  public Setup() {
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, PlaceUnits placeunits) {
    int senderID = placeunits.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    Capturable cap = context.getBoard().findCapturableByName(placeunits.capturablename);

    if (cap == null) {
      connection.sendEvent(senderID, new IllegalAction("Not a capturable!"));
      return;
    }

    if (placeunits.amount != 1) {
      connection.sendEvent(senderID, new IllegalAction("You may place only one unit during setup phase!"));
      return;
    }

    // The player needs to own this capturable to place units on it
    if (cap.getOwner() != player) {
      connection.sendEvent(senderID, new IllegalAction("You do not own this capturable!"));
      return;
    }

    // Transfer units
    cap.placeUnits(player, 1);

    // Notify players
    connection.sendEventToAll(new SetupUnit(cap.getName()));

    context.nextTurn();

    if (context.getTurningPlayer().units == 0) {
      context.transitionState(new Reinforcing());
    }
  }
}

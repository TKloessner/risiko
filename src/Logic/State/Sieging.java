package Logic.State;

import Logic.Capturable;
import Logic.Player;
import Network.ConnectionHandler;
import Network.Messages.Command.Siege;
import Network.Messages.Event.GameWon;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.UnitSiege;

public class Sieging extends State {
  private final Capturable winner;
  private final Capturable loser;

  public Sieging(Capturable winner, Capturable loser) {
    this.winner = winner;
    this.loser = loser;
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, Siege siege) {
    int senderID = siege.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    int units = siege.units;

    if (units == 0 || winner.hasUnits(units + 1)) {
      connection.sendEvent(senderID, new IllegalAction("Invalid number of siege units!"));
      return;
    }

    loser.siegeUnits(winner, units);
    context.incrementCaptures();

    connection.sendEventToAll(new UnitSiege(units));

    Player winner = context.getBoard().getWinner();
    if (winner != null) {
      // Send winner
      connection.sendEventToAll(new GameWon(winner.ID));

      // Shutdown connections
      connection.shutdownConnections();

      context.transitionState(null); // Game ends
    } else {
      context.transitionState(new Capturing());
    }
  }
}

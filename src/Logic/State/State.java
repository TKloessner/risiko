package Logic.State;

import Network.ConnectionHandler;
import Network.Messages.Command.*;
import Network.Messages.Event.GameInfo;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.PlayerLeft;

public abstract class State {
  public void handle(ServerModel context, ConnectionHandler connection, Attack attack) {
    connection.sendEvent(attack.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, ConnectionEstablished established) {
    int ID = established.getSenderID();
    connection.sendEvent(ID, new GameInfo(context.getPlayerLimit(), ID, context.getBoard(), context.getDeckDescription()));
  }

  public void handle(ServerModel context, ConnectionHandler connection, ConnectionFailed fail) {
    System.err.println("Connection " + fail.getSenderID() + " has failed.");
    connection.connect(fail.getSenderID()); // reconnect
  }

  public void handle(ServerModel context, ConnectionHandler connection, Defend defend) {
    connection.sendEvent(defend.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, EndCapturing endcapturing) {
    connection.sendEvent(endcapturing.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, EndTurn endturn) {
    connection.sendEvent(endturn.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, JoinRequest joinrequest) {
    connection.sendEvent(joinrequest.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, MoveUnits moveunits) {
    connection.sendEvent(moveunits.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, NoTrade moveunits) {
    connection.sendEvent(moveunits.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, PlaceUnits placeunits) {
    connection.sendEvent(placeunits.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, PlayerDisconnection playerdisconnection) {
    context.removePlayerWithID(playerdisconnection.getSenderID());
    connection.removeConnection(playerdisconnection.getSenderID());
    connection.sendEventToAll(new PlayerLeft(playerdisconnection.getSenderID()));

    if (context.getConnectedPlayers() == 0) {
      System.out.println("All players disconnected. The server will now close.");
      context.transitionState(null);
    }
  }

  public void handle(ServerModel context, ConnectionHandler connection, Siege siege) {
    connection.sendEvent(siege.getSenderID(), new IllegalAction(""));
  }

  public void handle(ServerModel context, ConnectionHandler connection, Trade playerdisconnection) {
    connection.sendEvent(playerdisconnection.getSenderID(), new IllegalAction(""));
  }
}

package Logic.State;

import Logic.Board;
import Logic.Capturable;
import Logic.Player;
import Network.ConnectionHandler;
import Network.Messages.Command.JoinRequest;
import Network.Messages.Command.PlayerDisconnection;
import Network.Messages.Event.*;

public class Lobby extends State {
  @Override
  public void handle(ServerModel context, ConnectionHandler connection, JoinRequest joinrequest) {
    int ID = joinrequest.getSenderID();
    String playername = joinrequest.playername;

    for (int i = 0; i < context.getPlayerLimit(); ++i) {
      Player p = context.getPlayerByID(i);
      if (p != null) {
        connection.sendEvent(ID, new PlayerJoined(i, p.getName()));
        connection.sendEvent(i, new PlayerJoined(ID, playername));
      }
    }

    context.addNewPlayer(ID, playername);
    connection.sendEvent(ID, new PlayerJoined(ID, playername));

    if (context.isPlayerLimitReached()) {
      System.out.println("All players connected. Starting game...");

      // Shuffle capturables and decks
      context.shuffleCapturables();
      context.shuffleDeck();

      // Give every player capturables and units
      distributeInitialUnits(context, connection);
      distributeCapturables(context, connection);

      // Notify game start
      connection.sendEventToAll(new GameStart());
      context.transitionState(new Setup());
    }
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, PlayerDisconnection playerdisconnection) {
    context.removePlayerWithID(playerdisconnection.getSenderID());
    connection.removeConnection(playerdisconnection.getSenderID());
    connection.sendEventToAll(new PlayerLeft(playerdisconnection.getSenderID()));

    connection.connect(playerdisconnection.getSenderID());
  }

  private void distributeCapturables(ServerModel context, ConnectionHandler connection) {
    Board board = context.getBoard();

    for (Capturable cap : board.Nodes()) {
      Player p = context.getTurningPlayer();

      // Assign capturable
      cap.give(p);

      connection.sendEventToAll(new CapturableDrawn(p.ID, cap.getName()));

      // Next player
      context.nextTurn();
    }
  }

  private void distributeInitialUnits(ServerModel context, ConnectionHandler connection) {
    // Give the players (2 * total areas / player number) units
    int units = (2 * context.getBoard().getNumNodes()) / context.getPlayerLimit();

    for (int i = 0; i < context.getPlayerLimit(); ++i) {
      Player p = context.getPlayerByID(i);
      if (p != null) {
        p.units = units;
        connection.sendEventToAll(new ReceivedStartingUnits(units, p.ID));
      }
    }
  }
}

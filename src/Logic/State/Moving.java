package Logic.State;

import Logic.Board;
import Logic.Capturable;
import Logic.Player;
import Network.ConnectionHandler;
import Network.Messages.Command.EndTurn;
import Network.Messages.Command.MoveUnits;
import Network.Messages.Event.BaseReinforcements;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.UnitMove;

public class Moving extends State {
  public Moving() {

  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, EndTurn endTurn) {
    int senderID = endTurn.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    context.nextTurn();

    Player p = context.getTurningPlayer();

    int territorial = Math.min(3, p.getNumCapturables() / 3);
    int continental = 0; // TODO

    connection.sendEventToAll(new BaseReinforcements(territorial, continental));

    context.transitionState(new Trading());
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, MoveUnits moveunits) {
    int senderID = moveunits.getSenderID();
    Player player = context.getPlayerByID(senderID);

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    Board board = context.getBoard();
    Capturable origin = board.findCapturableByName(moveunits.originname);
    Capturable target = board.findCapturableByName(moveunits.targetname);
    int amount = moveunits.amount;

    // Assure origin and target exist
    if (origin == null || target == null) {
      connection.sendEvent(player, new IllegalAction("Non-existent capturable!"));
      return;
    }

    // Capturables need to be connected
    if (board.getOwnerPath(origin, target, player) == null) {
      connection.sendEvent(player, new IllegalAction("No path from origin to target over your capturables!"));
      return;
    }

    // Don't surpass the unit count
    if (!origin.hasUnits(amount)) {
      connection.sendEvent(player, new IllegalAction("Trying to move more units than you have!"));
      return;
    }

    // Move the units
    origin.moveUnits(target, amount);

    // Send unit move
    connection.sendEvent(player, new UnitMove(origin.getName(), target.getName(), amount));
  }
}

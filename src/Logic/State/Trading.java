package Logic.State;

import Logic.Player;
import Logic.Card;
import Network.ConnectionHandler;
import Network.Messages.Command.NoTrade;
import Network.Messages.Command.Trade;
import Network.Messages.Event.BonusReinforcements;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.TradeDeclined;

import java.util.ArrayList;
import java.util.Collections;

public class Trading extends State {
  public Trading() {
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, NoTrade notrade) {
    int senderID = notrade.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    connection.sendEventToAll(new TradeDeclined());

    context.transitionState(new Reinforcing());
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, Trade tradecards) {
    int senderID = tradecards.getSenderID();
    Player player = context.getPlayerByID(senderID);

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    if (!player.hasCards(tradecards.cards)) {
      connection.sendEvent(senderID, new IllegalAction("Trade request is illegal!"));
      return;
    }

    for (int i = 0; i < 3; ++i) {
      player.removeCard(tradecards.cards[i]);
    }

    ArrayList<Card> trade = new ArrayList<>();

    for (int i = 0; i < 3; ++i) {
      Card card = tradecards.cards[i];
      if (card != Card.JOKER) {
        trade.add(card);
      }
    }

    Collections.sort(trade);

    int cardbonus;

    if (trade.stream().distinct().count() == trade.size()) {
      cardbonus = 12;
    } else if (trade.stream().allMatch((Card card) -> card == Card.CANNON)) {
      cardbonus = 9;
    } else if (trade.stream().allMatch((Card card) -> card == Card.CAVALRY)) {
      cardbonus = 6;
    } else if (trade.stream().allMatch((Card card) -> card == Card.INFANTRY)) {
      cardbonus = 3;
    } else {
      connection.sendEvent(senderID, new IllegalAction("This card combination can't be traded!"));
      return;
    }

    connection.sendEventToAll(new BonusReinforcements(cardbonus));

    context.transitionState(new Reinforcing());
  }
}

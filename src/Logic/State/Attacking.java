package Logic.State;

import Logic.Capturable;
import Network.ConnectionHandler;
import Network.Messages.Command.Defend;
import Network.Messages.Event.*;

import java.util.Arrays;

public class Attacking extends State {
  private final Capturable attacker;
  private final Capturable defender;
  private final int attackerunits;

  Attacking(Capturable attacker, Capturable defender, int attackerunits) {
    this.attacker = attacker;
    this.defender = defender;
    this.attackerunits = attackerunits;
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, Defend defend) {
    int senderID = defend.getSenderID();
    int defenderunits = defend.units;

    if (senderID != defender.getOwner().ID) {
      connection.sendEvent(senderID, new IllegalAction("You are not defending!"));
      return;
    }

    if (!defender.hasUnits(defenderunits)) {
      connection.sendEvent(senderID, new IllegalAction("You don't have that many units!"));
      return;
    }

    // Roll the dices and send the dice result
    int[] attackervalues = context.rollDices(attackerunits);
    int[] defendervalues = context.rollDices(defenderunits);

    Arrays.sort(attackervalues);
    Arrays.sort(defendervalues);

    connection.sendEventToAll(new DiceThrows(attackervalues, defendervalues));

    int attackerlost = 0;
    int defenderlost = 0;

    for (int i = 0; i < Math.min(attackerunits, defenderunits); ++i) {
      if (attackervalues[i] > defendervalues[i]) {
        ++defenderlost;
      } else {
        ++attackerlost;
      }
    }

    connection.sendEventToAll(new AttackFinished(attackerlost, defenderlost));

    attacker.killUnits(attackerlost);
    defender.killUnits(defenderlost);

    if (!defender.hasUnits()) {
      context.transitionState(new Sieging(attacker, defender));
    } else {
      context.transitionState(new Capturing());
    }
  }
}

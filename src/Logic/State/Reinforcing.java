package Logic.State;

import Logic.Capturable;
import Logic.Player;
import Network.ConnectionHandler;
import Network.Messages.Command.PlaceUnits;
import Network.Messages.Event.IllegalAction;
import Network.Messages.Event.UnitsPlaced;

public class Reinforcing extends State {
  public Reinforcing() {
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, PlaceUnits placeunits) {
    int senderID = placeunits.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    Capturable cap = context.getBoard().findCapturableByName(placeunits.capturablename);

    if (cap == null) {
      connection.sendEvent(senderID, new IllegalAction("Non-existent capturable!"));
      return;
    }

    // The player needs to own this capturable to place units on it
    if (cap.getOwner() != player) {
      connection.sendEvent(senderID, new IllegalAction("You do not own this capturable!"));
      return;
    }

    int amount = placeunits.amount;

    // Needs to have enough units
    if (player.units < amount) {
      connection.sendEvent(senderID, new IllegalAction("You do not have the amount of units specified!"));
      return;
    }

    // Notify players
    connection.sendEventToAll(new UnitsPlaced(amount, cap.getName()));

    // Transfer units
    cap.placeUnits(player, amount);

    if (player.units == 0) {
      context.transitionState(new Capturing());
    }
  }
}

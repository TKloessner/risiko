package Logic.State;

import Logic.*;
import Network.ConnectionHandler;
import Network.Messages.Command.Attack;
import Network.Messages.Command.EndCapturing;
import Network.Messages.Event.*;

public class Capturing extends State {
  public Capturing() {
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, Attack attack) {
    int senderID = attack.getSenderID();
    Board board = context.getBoard();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    Capturable origin = board.findCapturableByName(attack.attackername);
    Capturable target = board.findCapturableByName(attack.targetname);
    int units = attack.units;

    // Assert the capturables exist
    if (origin == null || target == null) {
      connection.sendEvent(senderID, new IllegalAction("Non-existent capturable!"));
      return;
    }

    // Origin must be owned
    if (origin.getOwner().ID != senderID) {
      connection.sendEvent(senderID, new IllegalAction("You do not own this capturable!"));
      return;
    }

    // Target must be owned by enemy
    if (target.getOwner().ID == senderID) {
      connection.sendEvent(senderID, new IllegalAction("You cannot attack your own capturable!"));
      return;
    }

    // Target needs to be adjacent to be attacked
    if (!board.areAdjacent(origin, target)) {
      connection.sendEvent(senderID, new IllegalAction("Your target is not adjacent!"));
      return;
    }

    // Origin needs to have enough units
    if (!origin.hasUnits(units + 1)) {
      connection.sendEvent(senderID, new IllegalAction("You do not have enough units!"));
      return;
    }

    // Send events to notify of the attempted capture
    connection.sendEventToAll(new AttackDeclared(origin.getName(), target.getName(), units));

    context.transitionState(new Attacking(origin, target, units));
  }

  @Override
  public void handle(ServerModel context, ConnectionHandler connection, EndCapturing endcapturing) {
    int senderID = endcapturing.getSenderID();
    Player player = context.getTurningPlayer();

    if (player != context.getPlayerByID(senderID)) {
      connection.sendEvent(senderID, new IllegalAction("It is not your turn!"));
      return;
    }

    // Send end capturing
    connection.sendEventToAll(new CapturePhaseEnd());

    if (context.getCaptures() != 0) {
      Card card = context.drawCard();
      player.addCard(card);
      connection.sendEvent(senderID, new CardDrawn(card));
      connection.sendEventToAll(new UnknownCardDrawn());
    }

    context.transitionState(new Moving());
  }
}

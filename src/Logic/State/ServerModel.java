package Logic.State;

import Logic.*;
import Network.ConnectionHandler;
import Network.Messages.Command.*;

import java.util.Collections;
import java.util.Random;

public class ServerModel extends GameModel {
  private Random rng;
  private State state;
  private Deck deck;

  public ServerModel(int players, long seed, Board board, DeckInfo deckdescription) {
    this(players, seed, board, deckdescription, new Lobby());
  }

  public ServerModel(int players, long seed, Board board, DeckInfo deckdescription, State initialstate) {
    super(players, board);
    this.rng = new Random(seed);
    this.deck = new Deck(deckdescription);
    this.state = initialstate;
  }

  DeckInfo getDeckDescription() {
    return deck.getDescription();
  }

  Card drawCard() {
    return deck.draw();
  }

  void shuffleCapturables() {
    Collections.shuffle(board.Nodes(), rng);
  }

  void shuffleDeck() {
    deck.shuffle(rng);
  }

  int[] rollDices(int numdices) {
    int[] rolls = new int[numdices];

    for (int i = 0; i < numdices; ++i) {
      rolls[i] = rng.nextInt(6) + 1;
    }

    return rolls;
  }

  void transitionState(State state) {
    this.state = state;
  }

  boolean isPlayerLimitReached() {
    return turnids.size() == players.length;
  }

  public boolean isFinished() {
    return state == null;
  }

  public void handle(ConnectionHandler connection, Attack event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, ConnectionEstablished event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, ConnectionFailed event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, Defend event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, EndCapturing event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, EndTurn event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, JoinRequest event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, MoveUnits event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, NoTrade event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, PlaceUnits event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, PlayerDisconnection event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, Siege event) {
    state.handle(this, connection, event);
  }

  public void handle(ConnectionHandler connection, Trade event) {
    state.handle(this, connection, event);
  }
}

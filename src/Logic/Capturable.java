package Logic;

import java.io.Serializable;

public class Capturable implements Serializable {
  private final String name;
  private Player owner;
  private int units;

  public Capturable(String name) {
    this.name = name;
    this.owner = null;
    this.units = 0;
  }

  public String getName() {
    return name;
  }

  public Player getOwner() {
    return owner;
  }

  public int getUnits() {
    return units;
  }

  public boolean hasUnits() {
    return hasUnits(1);
  }

  public boolean hasUnits(int units) {
    return this.units >= units;
  }

  public void placeUnits(Player p, int place) {
    assert(owner == p);
    assert(units > 0);
    transferUnits(p, place);
  }

  public void siegeUnits(Capturable from, int siegers) {
    assert(owner == null);
    assert(units == 0);

    setOwner(from.getOwner());
    moveUnits(from, siegers);
  }

  public void give(Player p) {
    setOwner(p);
    units = 1;
  }

  public void killUnits(int kill) {
    assert(hasUnits(kill));
    units -= kill;

    if (units == 0) {
      owner.removeCapturable(this);
      owner = null;
    }
  }

  public void moveUnits(Capturable destination, int movers) {
    assert(hasUnits(units + 1));
    units -= movers;
    destination.units += movers;
  }

  private void transferUnits(Player p, int units) {
    this.units += units;
    p.units -= units;
  }

  private void setOwner(Player player) {
    assert(units == 0);
    assert(owner == null);
    player.addCapturable(this);
    owner = player;
  }

}
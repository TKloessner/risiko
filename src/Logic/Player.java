package Logic;

import java.util.*;

public class Player {
  private final String name;
  private List<Card> cards;
  private Set<Capturable> owns;

  public final int ID;
  public int units;

  public Player(int ID, String name) {
    this.ID = ID;
    this.cards = new ArrayList<>();
    this.owns = new HashSet<>();
    this.name = name;
    this.units = 0;
  }

  public void addCard(Card card) {
    cards.add(card);
  }

  public void addCapturable(Capturable cap) {
    owns.add(cap);
  }

  public void removeCard(Card card) {
    cards.remove(card);
  }

  public void removeCapturable(Capturable cap) {
    owns.remove(cap);
  }

  public boolean hasCards(Card[] trade) {
    for (Card card : Card.values()) {
      int tradefrequency = 0;

      for (Card traded : cards) {
        if (traded == card) {
          ++tradefrequency;
        }
      }

      if (Collections.frequency(cards, card) < tradefrequency) {
        return false;
      }
    }

    return true;
  }

  public int getNumCapturables() {
    return owns.size();
  }

  public String getName() {
    return name;
  }
}
package Logic;

import java.util.*;

public abstract class GameModel {
  protected Queue<Integer> turnids;
  protected Player[] players;
  protected Board board;
  protected int captures;

  public GameModel(int numplayers, Board board) {
    this.turnids = new ArrayDeque<>(numplayers);
    this.players = new Player[numplayers];

    for (int i = 0; i < numplayers; ++i) {
      players[i] = null;
    }

    this.board = board;
    this.captures = 0;
  }

  public int getPlayerLimit() {
    return players.length;
  }

  public int getConnectedPlayers() {
    return turnids.size();
  }

  public Player getPlayerByID(int i) {
    return players[i];
  }

  public Player getTurningPlayer() {
    return players[turnids.peek()];
  }

  public Player addNewPlayer(int ID, String name) {
    turnids.add(ID);
    return players[ID] = new Player(ID, name);
  }

  public void removePlayerWithID(int ID) {
    turnids.remove(ID);
    players[ID] = null;
  }

  public void incrementCaptures() {
    ++captures;
  }

  public int getCaptures() {
    return captures;
  }

  public Board getBoard() {
    return board;
  }

  public void nextTurn() {
    captures = 0;
    turnids.offer(turnids.poll());
  }

}

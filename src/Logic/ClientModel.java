package Logic;

import Network.Messages.Event.*;

import java.util.ArrayList;
import java.util.List;

public class ClientModel extends GameModel {
  public int clientID;
  private List<ClientModelObserver> observers;
  private Capturable attacker, defender;

  public ClientModel(int players, Board board, int clientID) {
    super(players, board);
    this.observers = new ArrayList<>();
    this.clientID = clientID;
    this.attacker = null;
    this.defender = null;
  }

  public boolean isMyTurn() {
    return clientID == getTurningPlayer().ID;
  }

  public int getClientID() {
    return clientID;
  }

  public void register(ClientModelObserver observer) {
    observers.add(observer);
  }

  private void notifyAttackDeclaration(int units) {
    for (ClientModelObserver observer : observers) {
      observer.onAttackDeclaration(attacker, defender, units);
    }
  }

  private void notifyAttackCompleted() {
    for (ClientModelObserver observer : observers) {
      observer.onAttackCompletion(attacker, defender);
    }
  }

  private void notifyUnitsKilled(int attacking, int defending) {
    for (ClientModelObserver observer : observers) {
      observer.onUnitsKilled(attacker, defender, attacking, defending);
    }
  }

  private void notifyBaseReinforcements(int territorial, int continental) {
    for (ClientModelObserver observer : observers) {
      observer.onBaseReinforcements(territorial, continental);
    }
  }

  private void notifyBonusReinforcements(int cardbonus) {
    for (ClientModelObserver observer : observers) {
      observer.onBonusReinforcements(cardbonus);
    }
  }

  private void notifyCapturableDrawn(Player player, Capturable capturable) {
    for (ClientModelObserver observer : observers) {
      observer.onCapturableDrawn(player, capturable);
    }
  }

  private void notifyCapturePhaseEnd() {
    for (ClientModelObserver observer : observers) {
      observer.onCapturePhaseEnd();
    }
  }

  private void notifyCardDrawn(Card card) {
    for (ClientModelObserver observer : observers) {
      observer.onCardDrawn(card);
    }
  }

  private void notifyThrow(int[] attackervalues, int[] defendervalues) {
    for (ClientModelObserver observer : observers) {
      observer.onThrow(attackervalues, defendervalues);
    }
  }

  private void notifyGameStart() {
    for (ClientModelObserver observer : observers) {
      observer.onGameStart();
    }
  }

  private void notifyGameEnd(Player winner) {
    for (ClientModelObserver observer : observers) {
      observer.onGameEnd(winner);
    }
  }

  private void notifyIllegal() {
    for (ClientModelObserver observer : observers) {
      observer.onIllegal();
    }
  }

  private void notifyReceivedStartingUnits(Player p, int amount) {
    for (ClientModelObserver observer : observers) {
      observer.onReceivedStartingUnits(p, amount);
    }
  }

  private void notifyTradeDeclined() {
    for (ClientModelObserver observer : observers) {
      observer.onTradeDeclined();
    }
  }

  private void notifyUnitsMoved(Capturable from, Capturable to, int units) {
    for (ClientModelObserver observer : observers) {
      observer.onUnitsMoved(from, to, units);
    }
  }

  private void notifySiege(int units) {
    for (ClientModelObserver observer : observers) {
      observer.onSiege(attacker, defender, units);
    }
  }

  private void notifyUnitsPlaced(Capturable capturable, int units) {
    for (ClientModelObserver observer : observers) {
      observer.onUnitsPlaced(capturable, units);
    }
  }

  private void notifyAwaitSiege() {
    for (ClientModelObserver observer : observers) {
      observer.onAwaitSiege(attacker, defender);
    }
  }

  private void notifyPlayerJoined(Player player) {
    for (ClientModelObserver observer : observers) {
      observer.onPlayerJoined(player);
    }
  }

  private void notifyPleaseTrade() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseTrade();
    }
  }

  private void notifyPleaseReinforce() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseReinforce();
    }
  }

  private void notifyPleaseCapture() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseCapture();
    }
  }

  private void notifyPleaseSiege() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseSiege(attacker, defender);
    }
  }

  private void notifyPleaseMove() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseMove();
    }
  }

  private void notifyPleaseDefend() {
    for (ClientModelObserver observer : observers) {
      observer.onPleaseDefend(attacker, defender);
    }
  }

  public void handle(AttackDeclared event) {
    attacker = board.findCapturableByName(event.capfrom);
    defender = board.findCapturableByName(event.capagainst);
    notifyAttackDeclaration(event.units);

    if (defender.getOwner().ID == clientID) {
      notifyPleaseDefend();
    }
  }

  public void handle(AttackFinished event) {
    attacker.killUnits(event.attackerUnitsLost);
    defender.killUnits(event.defenderUnitsLost);

    notifyUnitsKilled(event.attackerUnitsLost, event.defenderUnitsLost);

    if (!defender.hasUnits()) {
      if (isMyTurn()) {
        notifyPleaseSiege();
      }
    } else {
      attacker = null;
      defender = null;

      if (isMyTurn()) {
        notifyPleaseCapture();
      }
    }
  }

  public void handle(BaseReinforcements event) {
    getTurningPlayer().units += event.territorial + event.continental;
    notifyBaseReinforcements(event.territorial, event.continental);

    if (isMyTurn()) {
      notifyPleaseTrade();
    }
  }

  public void handle(BonusReinforcements event) {
    getTurningPlayer().units += event.cardbonus;
    notifyBonusReinforcements(event.cardbonus);

    if (isMyTurn()) {
      notifyPleaseReinforce();
    }
  }

  public void handle(CapturableDrawn event) {
    Player p = getPlayerByID(event.ID);
    Capturable c = board.findCapturableByName(event.capturable);

    c.give(p);
    notifyCapturableDrawn(p, c);
  }

  @SuppressWarnings("unused")
  public void handle(CapturePhaseEnd event) {
    notifyCapturePhaseEnd();

    if (isMyTurn()) {
      notifyPleaseMove();
    }
  }

  public void handle(CardDrawn event) {
    getTurningPlayer().addCard(event.card);
    notifyCardDrawn(event.card);
  }

  public void handle(DiceThrows event) {
    notifyThrow(event.attackervalues, event.defendervalues);
  }

  @SuppressWarnings("unused")
  public void handle(GameStart event) {
    notifyGameStart();
  }

  public void handle(GameWon event) {
    notifyGameEnd(getPlayerByID(event.winnerID));
  }

  @SuppressWarnings("unused")
  public void handle(IllegalAction event) {
    notifyIllegal();
  }

  public void handle(PlayerJoined event) {
    notifyPlayerJoined(addNewPlayer(event.ID, event.name));
  }

  public void handle(PlayerLeft event) {
    removePlayerWithID(event.ID);
  }

  public void handle(ReceivedStartingUnits event) {
    Player p = getPlayerByID(event.recipientID);
    p.units += event.amount;

    notifyReceivedStartingUnits(p, event.amount);
  }

  public void handle(SetupUnit event) {
    Capturable cap = board.findCapturableByName(event.capturable);
    Player p = getTurningPlayer();
    cap.placeUnits(p, 1);
    nextTurn();

    notifyUnitsPlaced(cap, 1);

    if (isMyTurn()) {
      if (getPlayerByID(clientID).units != 0) {
        notifyPleaseReinforce();
      } else {
        notifyPleaseCapture();
      }
    }
  }

  @SuppressWarnings("unused")
  public void handle(TradeDeclined event) {
    notifyTradeDeclined();

    if (isMyTurn()) {
      notifyPleaseReinforce();
    }
  }

  public void handle(UnitMove event) {
    Capturable a = board.findCapturableByName(event.origin);
    Capturable b = board.findCapturableByName(event.target);
    a.moveUnits(b, event.amount);

    notifyUnitsMoved(a, b, event.amount);

    if (isMyTurn()) {
      notifyPleaseMove();
    }
  }

  public void handle(UnitSiege event) {
    defender.siegeUnits(attacker, event.units);
    notifySiege(event.units);

    attacker = null;
    defender = null;

    if (isMyTurn()) {
      notifyPleaseCapture();
    }
  }

  public void handle(UnitsPlaced event) {
    Capturable cap = board.findCapturableByName(event.cap);
    Player p = getTurningPlayer();
    cap.placeUnits(p, event.amount);

    notifyUnitsPlaced(cap, event.amount);

    if (isMyTurn()) {
      if (getPlayerByID(clientID).units != 0) {
        notifyPleaseReinforce();
      } else {
        notifyPleaseCapture();
      }
    }
  }

  @SuppressWarnings("unused")
  public void handle(UnknownCardDrawn event) {
    getTurningPlayer().addCard(null);
    notifyCardDrawn(null);
  }
}

package Utility;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

public class Graph<T> implements Serializable {
  protected List<T> nodes;
  protected Map<T, List<T>> successormap;

  public Graph() {
    nodes = new ArrayList<>();
    successormap = new HashMap<>();
  }

  public void addNode(T node) {
    nodes.add(node);
    successormap.putIfAbsent(node, new ArrayList<>());
  }

  public void addSuccessorToNode(T node, T successor) {
    successormap.get(node).add(successor);
  }

  public int getNumNodes() {
    return nodes.size();
  }

  public List<T> Nodes() {
    return nodes;
  }

  public List<T> Successors(T node) {
    return successormap.get(node);
  }

  public boolean DFS(T start, T end, Function<T, Boolean> f) {
    Set<T> visited = new HashSet<>();
    Stack<T> stack = new Stack<>();
    stack.push(start);

    while (!stack.empty()) {
      T node = stack.pop();
      visited.add(node);

      if (node == end) {
        return true;
      }

      for (T post : Successors(node)) {
        if (f.apply(post) && !visited.contains(post)) {
          stack.push(post);
        }
      }
    }

    return false;
  }
}

package GUI;

import GUI.Utility.Selection;
import GUI.Utility.SelectionChangeListener;
import Logic.Board;
import Logic.Capturable;
import Network.ClientConnection;
import Network.Messages.Command.PlaceUnits;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;

public class BoardView {
  private interface InputStrategy {
    void onClick(MouseEvent event, ImageView imageview);
  }

  private class PlaceStrategy implements InputStrategy {
    PlaceStrategy() { }

    @Override
    public void onClick(MouseEvent event, ImageView imageView) {
      Capturable capturable = board.findCapturableByName(imageView.getId());
      connection.enqueueMessage(new PlaceUnits(capturable.getName(), 1));
    }
  }

  private class AttackStrategy implements InputStrategy {
    private Selection<Pair<Capturable, ImageView>> selection;

    AttackStrategy() {
      this.selection = new Selection<>(2);
      selection.setSelectionChangeListener(new SelectionChangeListener<Pair<Capturable, ImageView>>() {
        @Override
        public void onSelection(Pair<Capturable, ImageView> object) {
          object.getValue().setEffect(click);
        }

        @Override
        public void onDeselection(Pair<Capturable, ImageView> object) {
          object.getValue().setEffect(null);
        }
      });
    }

    @Override
    public void onClick(MouseEvent event, ImageView imageView) {
      Capturable capturable = board.findCapturableByName(imageView.getId());
      selection.toggle(new Pair<>(capturable, imageView));

      if (selection.complete()) {
        Platform.runLater(() -> {
          Capturable attacker = selection.get(0).getKey();
          Capturable defender = selection.get(1).getKey();
          Image attackerimage = selection.get(0).getValue().getImage();
          Image defenderimage = selection.get(1).getValue().getImage();

          selection.reset();
          contextualDialogue.showOnAttack(attacker, defender, attackerimage, defenderimage);
        });
      }
    }
  }

  private class MoveStrategy implements InputStrategy {
    private Selection<Pair<Capturable, ImageView>> selection;

    MoveStrategy() {
      this.selection = new Selection<>(2);
      selection.setSelectionChangeListener(new SelectionChangeListener<Pair<Capturable, ImageView>>() {
        @Override
        public void onSelection(Pair<Capturable, ImageView> object) {
          object.getValue().setEffect(click);
        }

        @Override
        public void onDeselection(Pair<Capturable, ImageView> object) {
          object.getValue().setEffect(null);
        }
      });
    }

    @Override
    public void onClick(MouseEvent event, ImageView imageView) {
      Capturable capturable = board.findCapturableByName(imageView.getId());
      selection.toggle(new Pair<>(capturable, imageView));

      if (selection.complete()) {
        Platform.runLater(() -> {
          Capturable attacker = selection.get(0).getKey();
          Capturable defender = selection.get(1).getKey();
          Image attackerimage = selection.get(0).getValue().getImage();
          Image defenderimage = selection.get(1).getValue().getImage();

          selection.reset();
          contextualDialogue.showOnMove(attacker, defender, attackerimage, defenderimage);
        });
      }
    }
  }

  private final Board board;
  private final ClientConnection connection;

  private final ColorAdjust mouseover, click;

  private final AttackStrategy attackStrategy;
  private final PlaceStrategy placeStrategy;
  private final MoveStrategy moveStrategy;

  private InputStrategy inputStrategy;

  private Pane pane;
  private ContextualDialogue contextualDialogue;

  private static final Color[] colors = {
      Color.RED,
      Color.BLUE,
      Color.GREEN,
      Color.YELLOW,
      Color.PURPLE,
      Color.TURQUOISE,
      Color.BROWN,
      Color.WHITE
  };

  BoardView(Stage parent, BorderPane root, Board board, ClientConnection connection) throws IOException {
    this.board = board;
    this.connection = connection;

    this.attackStrategy = new AttackStrategy();
    this.placeStrategy  = new PlaceStrategy();
    this.moveStrategy   = new MoveStrategy();

    this.inputStrategy = placeStrategy;

    FXMLLoader fxmlLoader = new FXMLLoader(ClassLoader.getSystemClassLoader().getResource("default.fxml"));
    fxmlLoader.setController(this);
    this.pane = fxmlLoader.load();
    this.pane.setDisable(true);

    this.mouseover = new ColorAdjust(0.0, 0.0, 0.2, 0.0);
    this.click = new ColorAdjust(0.0,0.0, 0.4, 0.0);

    this.contextualDialogue = new ContextualDialogue(parent, connection);

    root.setCenter(pane);
  }

  void onDefend(Capturable attacker, Capturable defender) {
    Image attackerimage = getImageView(attacker).getImage();
    Image defenderimage = getImageView(defender).getImage();
    contextualDialogue.showOnDefense(attacker, defender, attackerimage, defenderimage);
  }

  void onSiege(Capturable attacker, Capturable defender) {
    Image attackerimage = getImageView(attacker).getImage();
    Image defenderimage = getImageView(defender).getImage();
    contextualDialogue.showOnSiege(attacker, defender, attackerimage, defenderimage);
  }

  void update(Capturable capturable) {
    Platform.runLater(() -> {
      Label label = getLabel(capturable);
      label.setText(Integer.toString(capturable.getUnits()));
      label.setTextFill(colors[capturable.getOwner().ID]);
    });
  }

  void disable() {
    pane.setDisable(true);
  }

  void enable() {
    pane.setDisable(false);
  }

  void setPlaceMode() {
    inputStrategy = placeStrategy;
    enable();
  }

  void setAttackMode() {
    inputStrategy = attackStrategy;
    enable();
  }

  void setMoveMode() {
    inputStrategy = moveStrategy;
    enable();
  }

  @FXML
  private void onMouseEnter(MouseEvent event) {
    ImageView entered = (ImageView) event.getSource();
    entered.setEffect(mouseover);
  }

  @FXML
  private void onMouseExit(MouseEvent event) {
    ImageView exited = (ImageView) event.getSource();
    exited.setEffect(null);
  }

  @FXML
  private void onClickCapturableImage(MouseEvent event) {
    if (inputStrategy != null) {
      inputStrategy.onClick(event, (ImageView) event.getSource());
    }
  }

  private Label getLabel(Capturable capturable) {
    return (Label) pane.lookup("#" + capturable.getName() + "_Label");
  }

  private ImageView getImageView(Capturable capturable) {
    return (ImageView) pane.lookup("#" + capturable.getName());
  }
}

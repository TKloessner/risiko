package GUI;

import Logic.*;
import Network.ClientConnection;
import Network.Messages.Event.GameInfo;
import Network.Messages.Message;
import Network.Messages.Terminate;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class GUIView implements ClientModelObserver {
  private static final double PLAYERSPACING = 30.0;

  private ClientModel model;

  private BorderPane root;

  private BoardView boardView;
  private CardView cardView;
  private ScoreBoard scoreBoard;

  public GUIView(Stage primaryStage, BlockingQueue<Message> queue, GameInfo gameInfo, ClientConnection connection, ClientModel model)
      throws IOException
  {
    this.model = model;

    this.root = new BorderPane();

    this.boardView = new BoardView(primaryStage, root, gameInfo.board, connection);
    this.scoreBoard = new ScoreBoard(root, PLAYERSPACING, gameInfo.players, gameInfo.board.getNumNodes());
    this.cardView = new CardView(root, connection);

    Platform.runLater(() -> {
      primaryStage.setTitle("Risiko");
      primaryStage.setOnCloseRequest((event) -> queue.offer(new Terminate()));
      primaryStage.setScene(new Scene(root));
      primaryStage.sizeToScene();
      primaryStage.centerOnScreen();
      primaryStage.setResizable(false);
      primaryStage.show();
    });
  }

  @Override
  public void onPleaseTrade() {

  }

  @Override
  public void onPleaseReinforce() {
    boardView.setPlaceMode();
  }

  @Override
  public void onPleaseCapture() {
    boardView.setAttackMode();
  }

  @Override
  public void onPleaseDefend(Capturable attacker, Capturable defender) {
    boardView.onDefend(attacker, defender);
  }

  @Override
  public void onPleaseSiege(Capturable attacker, Capturable defender) {
    boardView.onSiege(attacker, defender);
  }

  @Override
  public void onPleaseMove() {
    boardView.setMoveMode();
  }

  @Override
  public void onPlayerJoined(Player player) {
    scoreBoard.update(player);
  }

  @Override
  public void onAttackDeclaration(Capturable attacker, Capturable target, int units) {

  }

  @Override
  public void onAttackCompletion(Capturable attacker, Capturable defender) {

  }

  @Override
  public void onUnitsKilled(Capturable attacker, Capturable defender, int killedAttacking, int killedDefending) {
    boardView.update(attacker);
    boardView.update(defender);
  }

  @Override
  public void onBaseReinforcements(int territorial, int continental) {
    scoreBoard.update(model.getTurningPlayer());
  }

  @Override
  public void onBonusReinforcements(int bonus) {
    scoreBoard.update(model.getTurningPlayer());
  }

  @Override
  public void onCapturableDrawn(Player player, Capturable capturable) {
    boardView.update(capturable);
    scoreBoard.update(player);
  }

  @Override
  public void onCapturePhaseEnd() {

  }

  @Override
  public void onCardDrawn(Card card) {
    if (card != null) {
      cardView.addCard(card);
    }
  }

  @Override
  public void onThrow(int[] attackerthrows, int[] defenderthrows) {

  }

  @Override
  public void onGameStart() {
    if (model.isMyTurn()) {
      boardView.setPlaceMode();
    }
  }

  @Override
  public void onGameEnd(Player winner) {
    boardView.disable();
  }

  @Override
  public void onIllegal() {
    System.err.println("Illegal!");
  }

  @Override
  public void onReceivedStartingUnits(Player player, int amount) {
    scoreBoard.update(player);
  }

  @Override
  public void onTradeDeclined() {

  }

  @Override
  public void onUnitsMoved(Capturable source, Capturable destination, int units) {
    boardView.update(source);
    boardView.update(destination);
  }

  @Override
  public void onSiege(Capturable winner, Capturable loser, int units) {
    boardView.update(winner);
    boardView.update(loser);
  }

  @Override
  public void onUnitsPlaced(Capturable capturable, int units) {
    boardView.update(capturable);
  }

  @Override
  public void onAwaitSiege(Capturable attacker, Capturable defender) {

  }
}

package GUI;

import GUI.Utility.OverlappingGroup;
import GUI.Utility.Selection;
import GUI.Utility.SelectionChangeListener;
import Logic.Card;
import Network.ClientConnection;
import Network.Messages.Command.Trade;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class CardView {
  private static final double OVERLAP = 60.0;
  private static final ColorAdjust HOVERHIGLIGHT = new ColorAdjust(0.0, 0.0, 0.2, 0.0);
  private static final ColorAdjust CLICKHIGHLIGHT = new ColorAdjust(0.0, 0.0, 0.4, 0.0);

  private class CardImageView extends ImageView {
    boolean selected = false;
    Card card;

    CardImageView(Card card, Image image) {
      super(image);

      this.card = card;

      super.setOnMouseEntered((event) -> {
        if (!selected) {
          setEffect(HOVERHIGLIGHT);
        }
      });

      super.setOnMouseExited((event) -> {
        if (!selected) {
          setEffect(null);
        }
      });

      super.setOnMouseClicked((event) -> {
        if (selection.toggle(this)) {
          trade.setDisable(false);
        }
      });
    }
  }

  private static class ImageCache {
    private Image[] images = new Image[4];

    ImageCache(ClassLoader loader) {
      images[Card.INFANTRY.ordinal()] = new Image(loader.getResourceAsStream("Infantry.png"));
      images[Card.CAVALRY.ordinal()]  = new Image(loader.getResourceAsStream("Cavalry.png"));
      images[Card.CANNON.ordinal()]   = new Image(loader.getResourceAsStream("Cannon.png"));
      images[Card.JOKER.ordinal()]    = new Image(loader.getResourceAsStream("Joker.png"));
    }

    Image get(Card card) {
      return images[card.ordinal()];
    }
  }

  private ImageCache imagecache = new ImageCache(ClassLoader.getSystemClassLoader());
  private OverlappingGroup hand = new OverlappingGroup(OVERLAP);
  private Button trade = new Button("Trade");
  private Selection<CardImageView> selection = new Selection<>(3);

  CardView(BorderPane parent, ClientConnection connection) {
    trade.setPrefSize(80.0, 40.0);
    trade.setDisable(true);
    trade.setOnMouseClicked((event) -> {
      Card[] tradecards = new Card[3];

      for (int i = 0; i != 3; ++i) {
        CardImageView selected = selection.get(i);
        hand.remove(selected);
        tradecards[i] = selected.card;
      }

      selection.clear();

      connection.enqueueMessage(new Trade(tradecards));

      trade.setDisable(true);
    });

    StackPane handPane = new StackPane(hand);
    StackPane buttonPane = new StackPane(trade);
    handPane.setPrefHeight(230.0);
    buttonPane.setPrefHeight(80.0);

    parent.setBottom(new VBox(10.0, handPane, buttonPane));

    addCard(Card.CANNON);
    addCard(Card.CANNON);
    addCard(Card.CANNON);
    addCard(Card.CANNON);
    addCard(Card.CANNON);

    selection.setSelectionChangeListener(new SelectionChangeListener<CardImageView>() {
      @Override
      public void onSelection(CardImageView object) {
        object.setEffect(CLICKHIGHLIGHT);
        object.selected = true;
      }

      @Override
      public void onDeselection(CardImageView object) {
        trade.setDisable(true);
        object.setEffect(null);
        object.selected = false;
      }
    });
  }

  void addCard(Card card) {
    hand.add(new CardImageView(card, imagecache.get(card)));
  }
}

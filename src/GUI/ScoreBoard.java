package GUI;

import Logic.Player;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class ScoreBoard {
  private static final double SCOREBOARDWIDTH = 250.0;

  private PlayerDisplay[] playerDisplays;
  private int totalcapturables;

  private static final String[] colors = {
      "red", "blue", "green", "yellow",
      "purple", "turquoise", "brown", "white"
  };

  private static class PlayerDisplay extends HBox {
    private static final double GAP = 10.0;
    private static final double LABELWIDTH = 120.0;
    private static final double LABELHEIGHT = 10.0;
    private final Font namefont = Font.font("Verdana", 15);

    Label name = new Label("Connecting...");
    ProgressBar score = new ProgressBar(0.0);

    PlayerDisplay(String colorstyle) {
      super(GAP);

      name.setPrefSize(LABELWIDTH, LABELHEIGHT);
      name.setFont(namefont);
      score.setStyle("-fx-accent: " + colorstyle + ";");

      setAlignment(Pos.CENTER);
      getChildren().addAll(name, score);
    }
  }

  ScoreBoard(BorderPane pane, double spacing, int players, int totalcapturables) {
    this.totalcapturables = totalcapturables;
    this.playerDisplays = new PlayerDisplay[players];

    for (int i = 0; i != players; ++i) {
      playerDisplays[i] = new PlayerDisplay(colors[i]);
    }

    VBox vbox = new VBox(spacing, playerDisplays);
    vbox.setAlignment(Pos.CENTER);
    vbox.setPrefWidth(SCOREBOARDWIDTH);
    vbox.setVisible(true);

    pane.setRight(vbox);
  }

  void update(Player player) {
    Platform.runLater(() -> {
      PlayerDisplay display = playerDisplays[player.ID];
      display.name.setText(player.getName());
      display.score.setProgress((double) player.getNumCapturables() / (double) totalcapturables);
    });
  }
}

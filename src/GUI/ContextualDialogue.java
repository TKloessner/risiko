package GUI;

import Logic.Capturable;
import Network.ClientConnection;
import Network.Messages.Command.Attack;
import Network.Messages.Command.Defend;
import Network.Messages.Command.MoveUnits;
import Network.Messages.Command.Siege;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ContextualDialogue {
  // UI Controls
  @FXML private ImageView attackerImage;
  @FXML private ImageView defenderImage;
  @FXML private Spinner<Integer> unitsSpinner;
  @FXML private Button attackButton;
  private AnchorPane root;
  private Stage dialogueStage;

  private Stage parent;

  // Connection
  private ClientConnection connection;

  public ContextualDialogue(Stage parent, ClientConnection connection) throws IOException {
    this.connection = connection;
    this.parent = parent;

    FXMLLoader fxmlLoader = new FXMLLoader(ClassLoader.getSystemClassLoader().getResource("Attack.fxml"));
    fxmlLoader.setController(this);
    this.root = fxmlLoader.load();

    Platform.runLater(() -> {
      this.dialogueStage = new Stage();
      dialogueStage.initOwner(parent);
      dialogueStage.initModality(Modality.WINDOW_MODAL);
      dialogueStage.setScene(new Scene(root));
      dialogueStage.centerOnScreen();
      dialogueStage.sizeToScene();
    });
  }

  public void showOnAttack(Capturable attacker, Capturable defender, Image attackerimage, Image defenderimage) {
    SpinnerValueFactory<Integer> factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Math.min(3, attacker.getUnits() - 1), 1, 1);
    unitsSpinner.setValueFactory(factory);

    attackerImage.setImage(attackerimage);
    defenderImage.setImage(defenderimage);

    attackButton.setOnMouseClicked((event) -> {
      connection.enqueueMessage(new Attack(attacker.getName(), defender.getName(), unitsSpinner.getValue()));
      dialogueStage.close();
    });

    dialogueStage.toFront();
    dialogueStage.showAndWait();
  }

  public void showOnDefense(Capturable attacker, Capturable defender, Image attackerimage, Image defenderimage) {
    unitsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Math.min(2, defender.getUnits()), 1, 1));

    attackerImage.setImage(attackerimage);
    defenderImage.setImage(defenderimage);

    attackButton.setOnMouseClicked((event) -> {
      connection.enqueueMessage(new Defend(unitsSpinner.getValue()));
      dialogueStage.close();
    });

    dialogueStage.toFront();
    dialogueStage.showAndWait();
  }

  public void showOnMove(Capturable source, Capturable destination, Image attackerimage, Image defenderimage) {
    unitsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, source.getUnits() - 1, 1, 1));

    attackerImage.setImage(attackerimage);
    defenderImage.setImage(defenderimage);

    attackButton.setOnMouseClicked((event) -> {
      connection.enqueueMessage(new MoveUnits(source.getName(), destination.getName(), unitsSpinner.getValue()));
      dialogueStage.close();
    });

    dialogueStage.toFront();
    dialogueStage.showAndWait();
  }

  public void showOnSiege(Capturable attacker, Capturable defender, Image attackerimage, Image defenderimage) {
    unitsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Math.min(3, attacker.getUnits() - 1), 1, 1));

    attackerImage.setImage(attackerimage);
    defenderImage.setImage(defenderimage);

    attackButton.setOnMouseClicked((event) -> {
      connection.enqueueMessage(new Siege(unitsSpinner.getValue()));
      dialogueStage.close();
    });

    dialogueStage.toFront();
    dialogueStage.showAndWait();
  }
}

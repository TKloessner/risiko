package GUI.Utility;

import java.util.ArrayList;
import java.util.List;

public class Selection<T> {
  private SelectionChangeListener<T> selectionChangeListener = null;
  private int numselections;
  private List<T> selection;

  public Selection(int numselections) {
    this.numselections = numselections;
    this.selection = new ArrayList<>(numselections);
  }

  public void setSelectionChangeListener(SelectionChangeListener<T> selectionChangeListener) {
    this.selectionChangeListener = selectionChangeListener;
  }

  public boolean toggle(T object) {
    if (selection.indexOf(object) != -1) {
      selection.remove(object);
      if (selectionChangeListener != null) {
        selectionChangeListener.onDeselection(object);
      }
    } else if (selection.size() < numselections) {
      selection.add(object);
      if (selectionChangeListener != null) {
        selectionChangeListener.onSelection(object);
      }
    }

    return complete();
  }

  public boolean complete() {
    return selection.size() == numselections;
  }

  public T get(int i) {
    return selection.get(i);
  }

  public void reset() {
    for (T object : selection) {
      if (selectionChangeListener != null) {
        selectionChangeListener.onDeselection(object);
      }
    }

    clear();
  }

  public void clear() {
    selection.clear();
  }
}

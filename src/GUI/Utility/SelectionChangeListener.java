package GUI.Utility;

public interface SelectionChangeListener<T> {
  void onSelection(T object);
  void onDeselection(T object);
}

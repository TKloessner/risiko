package GUI.Utility;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;

public class OverlappingGroup extends Group {
  public OverlappingGroup(double overlap) {
    super();

    getChildren().addListener(new ListChangeListener<Node>() {
      @Override
      public void onChanged(Change<? extends Node> c) {
        ObservableList<? extends Node> list = c.getList();

        while (c.next()) {
          if (c.wasAdded() || c.wasRemoved()) {
            // Relocate all following elements
            for (int i = c.getFrom(); i != list.size(); ++i) {
              list.get(i).relocate(i * overlap, 0.0);
            }
          }
        }
      }
    });
  }

  // Convenience methods for list modification

  public void add(Node node) {
    getChildren().add(node);
  }

  public int indexOf(Node node) {
    return getChildren().indexOf(node);
  }

  public Node remove(int index) {
    return getChildren().remove(index);
  }

  public boolean remove(Node node) {
    return getChildren().remove(node);
  }
}
